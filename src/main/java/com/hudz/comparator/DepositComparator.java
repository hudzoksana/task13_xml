package com.hudz.comparator;

import com.hudz.model.Deposit;

import java.util.Comparator;

public class DepositComparator implements Comparator<Deposit> {
    @Override
    public int compare(Deposit o1, Deposit o2) {
        return Double.compare(o1.getAccountID(), o2.getAccountID());
    }
}
