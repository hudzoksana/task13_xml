package com.hudz.parser.sax;

import com.hudz.model.Deposit;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Deposit> depositList = new ArrayList<>();
    private Deposit deposit = null;
    private List<String> types = null;


    private boolean bName = false;
    private boolean bCountry = false;
    private boolean bTypes = false;
    private boolean bType = false;
    private boolean bDepositor = false;
    private boolean bAccountID = false;
    private boolean bAmountOfDeposit = false;
    private boolean bProfitability = false;
    private boolean bTimeConstrains = false;

    public List<Deposit> getDepositList(){
        return this.depositList;
    }

    public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("deposit")){
            deposit = new Deposit();
        }
        else if (qName.equalsIgnoreCase("name")){bName = true;}
        else if (qName.equalsIgnoreCase("country")){bCountry = true;}
        else if (qName.equalsIgnoreCase("types")){bTypes = true;}
        else if (qName.equalsIgnoreCase("type")){bType = true;}
        else if (qName.equalsIgnoreCase("depositor")){bDepositor = true;}
        else if (qName.equalsIgnoreCase("accountID")){bAccountID = true;}
        else if (qName.equalsIgnoreCase("amountOfDeposit")){bAmountOfDeposit = true;}
        else if (qName.equalsIgnoreCase("profitability")){bProfitability = true;}
        else if (qName.equalsIgnoreCase("timeConstrains")){bTimeConstrains = true;}
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("deposit")){
            depositList.add(deposit);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (bName){
            deposit.setName(new String(ch, start, length));
            bName = false;
        }
        else if (bCountry){
            deposit.setCountry(new String(ch, start, length));
            bCountry = false;
        }
        else if(bTypes){
            types = new ArrayList<>();
            bTypes = false;
        }
        else if(bType){
            String type= new String(ch, start, length);
            types.add(type);
            bType = false;
        }
        else if (bDepositor){
            deposit.setDepositor(new String(ch, start, length));
            bDepositor = false;
        }
        else if (bAccountID){
            deposit.setAccountID(Integer.parseInt(new String(ch, start, length)));
            bAccountID = false;
        }
        else if (bAmountOfDeposit){
            deposit.setAmountOfDeposit(Integer.parseInt(new String(ch, start, length)));
            bAmountOfDeposit = false;
        }
        else if (bProfitability){
            deposit.setProfitability(Double.parseDouble(new String(ch, start, length)));
            bProfitability = false;
        }
        else if (bTimeConstrains){
            deposit.setTimeConstrains(Double.parseDouble(new String(ch, start, length)));
            deposit.setType(types);
            bTimeConstrains = false;
        }
    }
}

