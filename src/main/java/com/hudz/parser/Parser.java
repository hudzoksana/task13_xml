package com.hudz.parser;

import com.hudz.comparator.DepositComparator;
import com.hudz.filechecker.ExtensionChecker;
import com.hudz.model.Deposit;
import com.hudz.parser.dom.DOMParserUser;
import com.hudz.parser.sax.SAXParserUser;
import com.hudz.parser.stax.StAXReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {
  private final static Logger logger = LogManager.getLogger(Parser.class);

  public static void main(String[] args) {
    File xml = new File("src//main//resources//xml//Banks.xml");
    File xsd = new File("src//main//resources//xml//Banks.xsd");

    if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            SAX
      printList(SAXParserUser.parseDeposit(xml, xsd), "SAX");
//
//            StAX
      printList(StAXReader.parseDeposit(xml, xsd), "StAX");

//            DOM
      printList(DOMParserUser.getDepositList(xml, xsd), "DOM");
    }
  }

  private static boolean checkIfXSD(File xsd) {
    return ExtensionChecker.isXSD(xsd);
  }

  private static boolean checkIfXML(File xml) {
    return ExtensionChecker.isXML(xml);
  }

  private static void printList(List<Deposit> deposits, String parserName) {
    Collections.sort(deposits, new DepositComparator());
    System.out.println(parserName);
    for (Deposit deposit : deposits) {
      logger.trace(deposit);
    }
  }

}
