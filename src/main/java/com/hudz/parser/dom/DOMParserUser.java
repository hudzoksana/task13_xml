package com.hudz.parser.dom;

import com.hudz.model.Deposit;
import org.w3c.dom.*;

import java.io.File;
import java.util.List;

public class DOMParserUser {
    public static List<Deposit> getDepositList(File xml, File xsd){
        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();

//        try {
//            DOMValidator.validate(DOMValidator.createSchema(xsd),doc);
//        }catch (IOException | SAXException ex){
//            ex.printStackTrace();
//        }

        DOMDocReader reader = new DOMDocReader();

        return reader.readDoc(doc);
    }





}
