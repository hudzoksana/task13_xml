package com.hudz.parser.dom;

import com.hudz.model.Deposit;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Deposit> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Deposit> deposits = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("deposit");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Deposit deposit = new Deposit();
            List<String> type;
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;

                deposit.setName(element.getElementsByTagName("name").item(0).getTextContent());
                deposit.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
                type = getTypes(element.getElementsByTagName("types"));
                deposit.setType(type);
                deposit.setName(element.getElementsByTagName("name").item(0).getTextContent());
                deposit.setDepositor(element.getElementsByTagName("depositor").item(0).getTextContent());
                deposit.setAccountID(Integer.parseInt(element.getElementsByTagName("accountID").item(0).getTextContent()));
                deposit.setAmountOfDeposit(Integer.parseInt(element.getElementsByTagName("amountOfDeposit").item(0).getTextContent()));
                deposit.setProfitability(Double.parseDouble(element.getElementsByTagName("profitability").item(0).getTextContent()));
                deposit.setTimeConstrains(Double.parseDouble(element.getElementsByTagName("timeConstrains").item(0).getTextContent()));
                deposits.add(deposit);

            }
        }
        return deposits;
    }
    private List<String> getTypes(NodeList nodes){
        List<String> types = new ArrayList<>();
        if(nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            NodeList nodeList = element.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE){
                    Element el = (Element) node;
                    types.add(el.getTextContent());
                }
            }
        }

        return types;
    }
}
