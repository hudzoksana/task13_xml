package com.hudz.parser.stax;

import com.hudz.model.Deposit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAXReader {
    public static List<Deposit> parseDeposit(File xml, File xsd){
        List<Deposit> depositList = new ArrayList<>();
        List<String> types = null;
        Deposit deposit = null;
        int i = 0;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "deposit":
                            deposit = new Deposit();
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert deposit != null;
                            deposit.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "country":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert deposit != null;
                            deposit.setCountry(xmlEvent.asCharacters().getData());
                            break;
                        case "types":
                            xmlEvent = xmlEventReader.nextEvent();
                            types = new ArrayList<>();
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert types != null;
                            types.add(xmlEvent.asCharacters().getData());
                            break;
                        case "depositor":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert deposit != null;
                            deposit.setDepositor(xmlEvent.asCharacters().getData());
                            break;
                        case "accountID":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert deposit != null;
                            deposit.setAccountID(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "amountOfDeposit":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert deposit != null;
                            deposit.setAmountOfDeposit(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "profitability":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert deposit != null;
                            deposit.setProfitability(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "timeConstrains":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert deposit != null;
                            deposit.setTimeConstrains(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            deposit.setType(types);
                            break;

                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("deposit")){
                        depositList.add(deposit);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return depositList;
    }
}
