<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: white">
                <div style="background-color: gray; color: black;">
                    <h2>Bank`s deposits</h2>
                </div>
                <table border="2">
                    <tr bgcolor="gray">
                        <th>Name</th>
                        <th>Country</th>
                        <th>Type:</th>
                        <th>Depositor</th>
                        <th>AccountID</th>
                        <th>Amount of deposit</th>
                        <th>Profitability</th>
                        <th>Time constrains</th>
                    </tr>

                    <xsl:for-each select="Banks/deposit">

                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="country"/></td>
                            <td>
                                <xsl:value-of select="types"/>
                                <xsl:for-each select="types">
                            </xsl:for-each>
                            </td>
                            <td><xsl:value-of select="depositor"/></td>
                            <td><xsl:value-of select="accountID"/></td>
                            <td><xsl:value-of select="amountOfDeposit"/></td>
                            <td><xsl:value-of select="profitability"/></td>
                            <td><xsl:value-of select="timeConstrains"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>